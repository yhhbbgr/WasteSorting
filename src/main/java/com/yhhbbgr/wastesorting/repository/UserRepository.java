package com.yhhbbgr.wastesorting.repository;

import com.yhhbbgr.wastesorting.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {


}
