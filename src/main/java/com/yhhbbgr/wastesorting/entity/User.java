package com.yhhbbgr.wastesorting.entity;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Data
@ApiModel
@Slf4j
@Accessors(chain = true)
@ToString(callSuper = true)
@Table(name = "user")
public class User extends BaseEntity{

    @Column(name = "username")
    private String  username;

    @Column(name = "account")
    private String  account;

    @Column(name = "password")
    private String  password;

    @Column(name = "score")
    private double  score;


}
