package com.yhhbbgr.wastesorting.config;



import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.http.HttpStatus;

import java.io.IOException;

/**
 * 功能描述：重写JSON序列化
 * @Auther: 郭榕
 * @Date: 2019/8/27 12:54
 */
public class FastJsonEnumSerializer extends JsonSerializer {

    @Override
    public void serialize(Object object, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        if (object instanceof HttpStatus) {
            HttpStatus httpStatus = (HttpStatus) object;
            jsonGenerator.writeNumber(httpStatus.value());
        }
    }

}
