package com.yhhbbgr.wastesorting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WastesortingApplication {

    public static void main(String[] args) {
        SpringApplication.run(WastesortingApplication.class, args);
    }

}
