package com.yhhbbgr.wastesorting.service;


import com.yhhbbgr.wastesorting.entity.User;

import java.util.List;

/**
 * 对用户业务操作的接口
 */
public interface UserService {

    public User findUserById(Long id);

    public List<User> findAll();

    public void add();
}
