package com.yhhbbgr.wastesorting.service;

import com.baidu.aip.imageclassify.AipImageClassify;
import org.json.JSONObject;

import java.util.HashMap;

public class ImageClassify {

    //设置APPID/AK/SK
    public static final String APP_ID = "17048904";
    public static final String API_KEY = "4evztqYl5B26vQTzGc5Uv6b8";
    public static final String SECRET_KEY = "qaR8x2qeACgpcN4NPZfutS3eNAd3TCHB";

    public static void main(String[] args) {
        // 初始化一个AipImageClassify
        AipImageClassify client = new AipImageClassify(APP_ID, API_KEY, SECRET_KEY);

        // 可选：设置网络连接参数
        client.setConnectionTimeoutInMillis(2000);
        client.setSocketTimeoutInMillis(60000);

        // 可选：设置代理服务器地址, http和socket二选一，或者均不设置
        //client.setHttpProxy("proxy_host", proxy_port);  // 设置http代理
        //client.setSocketProxy("proxy_host", proxy_port);  // 设置socket代理
        System.out.println(client.toString());
        // 调用识别方法
        sample(client);
    }

    public static void sample(AipImageClassify client) {
        // 传入可选参数调用接口
        HashMap<String, String> options = new HashMap<String, String>();
        options.put("top_num", "5");
        options.put("baike_num", "2");

        // 参数为本地路径
        //String imagePath = "D:/Rong Guo.jpg";
        //String imagePath = "D:/timg.jpg";
        String imagePath = "D:/hehua.jpg";
        //JSONObject res = client.carDetect(imagePath, options);
        //JSONObject res = client.objectDetect(imagePath, new HashMap<String, String>());
        JSONObject res = client.advancedGeneral(imagePath, new HashMap<String, String>());
       // JSONObject res = client.flower(imagePath, options);
        System.out.println(res.toString(2));

    }

}
