package com.yhhbbgr.wastesorting.service.serviceImpl;

import com.yhhbbgr.wastesorting.entity.User;
import com.yhhbbgr.wastesorting.repository.UserRepository;
import com.yhhbbgr.wastesorting.service.UserService;
import com.yhhbbgr.wastesorting.util.SnowflakeIdUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SnowflakeIdUtil snowflakeIdUtil;
    @Override
    public User findUserById(Long id) {
        return userRepository.findById(id).get();
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public void add(){
        for(int i = 0 ; i < 10 ; i++) {
            User user = new User();
            user.setAccount("yuwretwt"+i*100+i*10+i);
            user.setPassword("vxchgh"+i*100+i*10+i);
            user.setUsername("李四02"+i+i+i);
            user.setScore((double)i + 0.5 * i + 2.6 + i*i*0.1);
            user.setId(snowflakeIdUtil.nextId());
            user.setCreateBy(1L);
            user.setCreateAt(new Date());
            user.setUpdateBy(1L);
            user.setUpdateAt(new Date());
            userRepository.save(user);
        }
    }

}
