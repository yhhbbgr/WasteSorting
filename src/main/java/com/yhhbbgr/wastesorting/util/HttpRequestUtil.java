package com.yhhbbgr.wastesorting.util;

import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class HttpRequestUtil {

    /**
     * @author 郭榕
     * @date 2019/7/17 15:02
     * 功能描述:  封装GET请求方法
     * @param url
     * @return
     */
    public static String sendGetRequestToBank(String url){

        MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
        //params.add("questionCount" , 5);
        //params.add("tagIdLists" , list);
        RestTemplate restTemplate = new RestTemplate();
        //新建Http头，add方法可以添加参数
        HttpHeaders headers = new HttpHeaders();
        //设置请求发送方式
        HttpMethod method = HttpMethod.GET;
        // 以表单的方式提交
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        //将请求头部和参数合成一个请求
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(params, headers);
        //执行HTTP请求，将返回的结构使用String 类格式化（可设置为对应返回值格式的类）
        ResponseEntity<String> response = restTemplate.exchange(url, method, requestEntity,String.class);
        // JSONObject jsonObject = JSONObject.parseObject(response.getBody());
        return response.getBody();
    }
}

