package com.yhhbbgr.wastesorting.controller;


import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api")
@CrossOrigin
@Api("对战接口")
public class apiController {

    @RequestMapping(value = "/user",method = RequestMethod.GET)
    @ApiOperation(value="测试交互", notes="入参为name")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "用户名", required = true, dataType = "String",paramType = "path"),
    })
    public JSONObject user(String name , HttpServletResponse response){
        //response.setHeader("Access-Control-Allow-Origin", "*");
        //response.setHeader("Cache-Control","no-cache");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name","张三");
        jsonObject.put("key","001");
        jsonObject.put("password","87300");
        jsonObject.put("response",name);
        System.out.println("请求已收到:  " + name);
        return jsonObject;
    }

    @RequestMapping(value = "/user222",method = RequestMethod.POST)
    public JSONObject user222(@RequestBody String name ,HttpServletResponse response){
        //response.setHeader("Access-Control-Allow-Origin", "*");
        //response.setHeader("Cache-Control","no-cache");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name","张三");
        jsonObject.put("key","001");
        jsonObject.put("password","87300");
        jsonObject.put("response",name);
        System.out.println("请求已收到:  " + name);
        return jsonObject;
    }
}
