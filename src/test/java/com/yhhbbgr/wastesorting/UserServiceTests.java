package com.yhhbbgr.wastesorting;


import com.yhhbbgr.wastesorting.entity.User;
import com.yhhbbgr.wastesorting.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 用户业务测试类
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class UserServiceTests {

    @Autowired
    private UserService userService;

    @Test
    public void testFindUser(){
        //userService.findUserById(444L);
        for(User user : userService.findAll()){
            System.out.println(user);
        }


    }

    @Test
    public void testAddUser(){
        userService.add();
    }
}
