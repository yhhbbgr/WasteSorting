package com.yhhbbgr.wastesorting;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WastesortingApplicationTests {

    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private DataSourceProperties dataSourceProperties;
    @Test
    public void contextLoads() {
       System.out.println("Test~~~");
    }

    @Test
    public void testDataSource(){
        // 获取配置的数据源
        DataSource dataSource = applicationContext.getBean(DataSource.class);
        // 查看配置数据源信息
        System.out.println(dataSource);
        System.out.println(dataSource.getClass().getName());
        System.out.println(dataSourceProperties);
    }
}
