package com.yhhbbgr.wastesorting;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LogTests {

    //日志记录器
    //Slf4j
    Logger logger = LoggerFactory.getLogger(getClass());
    @Test
    public void testLogging(){
        //日志的级别
        //由低到高以下,越高级输出越少
        logger.trace("这是trace日志,轨迹.....");
        logger.debug("这是debug日志.....调试");
        logger.info("这是info日志.....打印");
        logger.warn("这是warn日志....警告");
        logger.error("这是error日志.....日常");


    }
}
